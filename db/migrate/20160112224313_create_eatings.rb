class CreateEatings < ActiveRecord::Migration
  def change
    create_table :eatings do |t|
      t.string :food, array: true
      t.references :user, index: true, foreign_key: true

      t.timestamps
    end
  end
end
