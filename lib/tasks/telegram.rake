require 'telegram_bot'

module Botan
  require 'net/http'
  require 'net/https'
  require 'json'

  URI_TEMPLATE = 'https://api.botan.io/track?token=%{token}&uid=%{uid}&name=%{name}';

  def self.track(token, uid, message, name = 'Message')
    begin
      uri = URI(URI_TEMPLATE % {token: token, uid: uid, name: name})
      puts uri
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      body = JSON.dump(message)

      req =  Net::HTTP::Post.new(uri)
      req.add_field "Content-type", "application/json"
      req.body = body
      # Fetch Request
      res = http.request(req)
      return JSON.parse(res.body)
    rescue StandardError => e
      puts "HTTP Request failed (#{e.message})"
    end
  end
end

namespace :telegram do
  task listen: :environment do
    bot = TelegramBot.new(token: ENV['TELEGRAM_TOKEN'])

    bot.get_updates(fail_silently: true) do |message|
      command = message.get_command_for(bot)
      command_name = command

      message.reply do |reply|
        puts message.inspect
        case command
        when /start/i
          reply.text = "Привет! Кажется, ты еще не зарегистрирован. \nОтправь /register <email> для регистрации"
        when /register/i
          telegram_id = message.from.id
          email = message.text.split(' ').last

          if !User.where(
            telegram_id: telegram_id,
            email: email,
          ).exists?
            password = Devise.friendly_token()

            user = User.create(email: email, password: password, telegram_id: telegram_id)

            if user.valid?
              reply.text = "Регистрация прошла успешно. Твой пароль: #{password}"
            else
              reply.text = "Ошибка: #{user.errors.full_messages.join(', ')}"
            end
          end
        when /greet/i
          reply.text = "Hello, #{message.from.first_name}!"
        else
          command_name = 'food'

          if user = User.find_by(telegram_id: message.from.id)
            if Eating.create(
              food: message.text.split(','),
              user: user
            )
              reply.text = "Записал!"
            else
              reply.text = "Что-то пошло не так :("
            end
          else
            reply.text = "Привет! Кажется, ты еще не зарегистрирован. \nОтправь /register <email> для регистрации"
          end
        end

        Botan.track(ENV['BOTAN_TOKEN'], message.from.id, message, command_name)

        reply.send_with(bot)
      end
    end

  end
end
