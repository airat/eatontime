Rails.application.routes.draw do
  namespace :admin do
    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      resources dashboard_resource
    end

    root controller: DashboardManifest::ROOT_DASHBOARD, action: :index
  end

  root 'eatings#index'

  get 'about', to: 'pages#about'

  resources :eatings, only: [:new, :create, :index, :edit, :update, :destroy]

  devise_for :users
end
