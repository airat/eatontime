class EatingsController < ApplicationController
  before_action :authenticate_user!

  def index
    @eatings = current_user.eatings.by_date
  end

  def edit
    @eating = Eating.find(params[:id])
  end

  def new
    @eating = Eating.new
  end

  def create
    Eating.create(eating_attributes.merge(user: current_user))

    redirect_to(root_path)
  end

  def update
    @eating = Eating.find(params[:id])

    @eating.update_attributes(eating_attributes)

    redirect_to(root_path)
  end

  def destroy
    @eating = Eating.find(params[:id])

    @eating.destroy

    redirect_to(root_path)
  end

  private

  def eating_attributes
    params.require(:eating).permit(:created_at, food: [])
  end
end
