class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :set_timezone

  def set_timezone
    min = request.cookies["timezone"].to_i
    Time.zone = ActiveSupport::TimeZone[-min.minutes]
  end
end
