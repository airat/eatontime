class Eating < ActiveRecord::Base
  scope :by_date, -> { order('created_at ASC') }

  before_save do
    self.food = food[0].split(',').map { |food_entry| food_entry.mb_chars.strip.capitalize.to_s }
  end

  belongs_to :user

  def day
    I18n.l(created_at, format: '%d %B')
  end
end
