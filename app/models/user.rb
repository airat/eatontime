class User < ActiveRecord::Base
  devise :database_authenticatable, :trackable, :validatable

  has_many :eatings

  validates :email, :encrypted_password, :telegram_id, presence: true
end
