module ApplicationHelper
  def card_color(eatings)
    case eatings.count
    when 5 then 'green'
    when 1..2 then 'red'
    when 3..4 then 'yellow'
    end
  end

  def eat_grade(eatings)
    case eatings.count
    when 5 then 'отлично'
    when 1..2 then 'плохо'
    when 3..4 then 'так себе'
    end
  end
end
